# Desafio DevOps - App Engine - Serverless #

Pipeline de CI\CD para implantação da aplicação de Comentários em versão API (backend) desenvolvida em Python.

### Quais ferramentas foram utilizadas no Desafio DevOps - App Engine ###

* Repositório Git: Bitbucket Cloud
* Orquestrador CI\CD: Bitbucket Pipeline - bitbucket-pipelines.yml
* Infrastructure as code: YAML setup ambiente Serverless - app.yaml
* Teste - Scanner de Qualidade e Segurança: [https://sonarcloud.io/](https://sonarcloud.io/dashboard?id=claudiomartinbianco6-admin_desafio-devops-app-engine)
* Recursos Serverless: App Engine
* Segurança: Service Account - integração dos ambientes
* Gatilho para gerar Release: Commit na branch Master
* Monitoramento: App Engine - Dashboard

### Deploy da aplicação ###

* [Link da aplicação - matéria 1](https://desafio-devops-325423.uc.r.appspot.com/api/comment/list/1)
* curl -sv https://desafio-devops-325423.uc.r.appspot.com/api/comment/list/1
* [Link da aplicação - matéria 2](https://desafio-devops-325423.uc.r.appspot.com/api/comment/list/2)
* curl -sv https://desafio-devops-325423.uc.r.appspot.com/api/comment/list/2

### Teste da aplicação - matéria 1 ###

* curl -sv https://desafio-devops-325423.uc.r.appspot.com/api/comment/new -X POST -H 'Content-Type: application/json' -d '{"email":"alice@example.com","comment":"first post!","content_id":1}'
* curl -sv https://desafio-devops-325423.uc.r.appspot.com/api/comment/new -X POST -H 'Content-Type: application/json' -d '{"email":"alice@example.com","comment":"ok, now I am gonna say something more useful","content_id":1}'
* curl -sv https://desafio-devops-325423.uc.r.appspot.com/api/comment/new -X POST -H 'Content-Type: application/json' -d '{"email":"bob@example.com","comment":"I agree","content_id":1}'

### Teste da aplicação - matéria 2 ###

* curl -sv https://desafio-devops-325423.uc.r.appspot.com/api/comment/new -X POST -H 'Content-Type: application/json' -d '{"email":"bob@example.com","comment":"I guess this is a good thing","content_id":2}'
* curl -sv https://desafio-devops-325423.uc.r.appspot.com/api/comment/new -X POST -H 'Content-Type: application/json' -d '{"email":"charlie@example.com","comment":"Indeed, dear Bob, I believe so as well","content_id":2}'
* curl -sv https://desafio-devops-325423.uc.r.appspot.com/api/comment/new -X POST -H 'Content-Type: application/json' -d '{"email":"eve@example.com","comment":"Nah, you both are wrong","content_id":2}'